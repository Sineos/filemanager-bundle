<?php
/*
 * This file is part of sineos-filemanager-bundle.
 *
 * (c) Oliver Lohoff, Contao4you.de
 *
 * @license LGPL-3.0-or-later
 */

namespace Sineos\FileManagerBundle\Controller;

use Contao\System;
use Contao\Database;
use Contao\CoreBundle\Framework\ContaoFramework;
use Contao\CoreBundle\Image\ImageFactoryInterface;
use Contao\CoreBundle\Image\Studio\Studio;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Contao\CoreBundle\Monolog\ContaoContext;
use Contao\File;
use Contao\FilesModel;
use Contao\Image\ResizeConfiguration;
use Contao\Input;
use Sineos\FileManagerBundle\Search\UsageFinder;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class FileUsage extends AbstractController
{
    protected $imageFactory;
    protected $params;
    protected $studio;

    public function __construct(
        ImageFactoryInterface $imageFactory, 
        ParameterBagInterface $params,
        Studio $studio
        )
    {
        $this->imageFactory = $imageFactory;
        $this->params = $params;
        $this->studio = $studio;
    }
    /**
     *
     * @param Request request the request object
     * @Route("/api/filemanager/usage/{id}", methods={"GET"}, name="usage")
     * @return string
     */
    public function usage(Request $request, $id, ContaoFramework $contao)
    {
        $contao->initialize();
        $usageFinder = new UsageFinder();
        $res = $usageFinder->findUsageById($id);
        $res["preview"] = "";

        //$container = System::getContainer();
        //$validImageExtensions = $container->getParameter('contao.image.valid_extensions');
        $projectRoot = $this->params->get('kernel.project_dir');
        $validImageExtensions = $this->params->get('contao.image.valid_extensions');

        if(in_array($res["extension"], $validImageExtensions)) {
            $path = $projectRoot . DIRECTORY_SEPARATOR . $res["path"];
            $res["preview"] = $this->generatePreviewImage($path);
        }
        

        return new JsonResponse($res, 200);
    }

    protected function generatePreviewImage($path)
    {
        $figureBuilder = $this->studio->createFigureBuilder();
        $figureBuilder
          ->fromPath($path)
          ->setSize([100, 100, 'crop']);

        $figure = $figureBuilder->build();
        return $figure->getImage()->getImageSrc();
    }

    /**
     *
     * @param Request request the request object
     * @Route("/api/filemanager/files", methods={"GET"}, name="files")
     * @return string
     */
    public function getFileIds(Request $request, ContaoFramework $contao)
    {
        $contao->initialize();

        $path = Input::get("path");
        
        $arrColumns = array();
        $arrColumns[] = "tl_files.path LIKE ? AND `type` = 'file'";
        $arrValues = array();
        $arrValues[] = $path;

        $filter = Input::get("filter");
        if(isset($filter) && $filter !== "") {
            $arrColumns[] = "tl_files.extension = ?";
            $arrValues[] = $filter;
        }

        $objFiles = FilesModel::findBy($arrColumns, $arrValues);

        if($objFiles == null) {
            return new JsonResponse([], 200);    
        }

        $fileIds = $objFiles->fetchEach("id");

        return new JsonResponse($fileIds, 200);
    }

    /**
     *
     * @param Request request the request object
     * @Route("/api/filemanager/file/{id}", methods={"DELETE"}, name="deletefile")
     * @return string
     */
    public function deletefile(Request $request, $id, ContaoFramework $contao)
    {
        $contao->initialize();
        $objFile = FilesModel::findById($id);
        $file = new File($objFile->path);
        $file->delete();
        $objFile->delete();

        return new JsonResponse("ok", 200);

    }
}