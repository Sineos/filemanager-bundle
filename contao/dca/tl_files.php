<?php
/*
 * This file is part of sineos-filemanager-bundle.
 *
 * (c) Oliver Lohoff, Contao4you.de
 *
 * @license LGPL-3.0-or-later
 */

$GLOBALS['TL_DCA']['tl_files']['list']['operations']['usage'] = array(
	'label'               => &$GLOBALS['TL_LANG']['tl_files']['usage'],
	'href'                => 'key=usage',
	'icon'                => 'bundles/filemanager/icons/link-2.svg',
	'button_callback' 	  => array('Sineos\FileManagerBundle\DataContainer\FileListener', "getUsageButton")
);

