<?php

use Sineos\FileManagerBundle\LicenseHelper;

$GLOBALS['TL_DCA']['tl_settings']['palettes']['default'] .= ';{sineos_filemanager_license_legend:hide},sineos_filemanager_license';

$GLOBALS['TL_DCA']['tl_settings']['fields']['sineos_filemanager_license'] = array(
	'label'     => &$GLOBALS['TL_LANG']['tl_settings']['sineos_filemanager_license'],
	'inputType' => 'text',
	'save_callback' => array(
        array(LicenseHelper::class, 'licenseSaveCallback'),
	)
);
